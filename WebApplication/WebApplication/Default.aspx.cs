﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient; //this namespace is for sqlclient server  
using System.Configuration; // this namespace is add I am adding connection name in web config file config connection name
using System.Data;
using System.Diagnostics;

namespace WebApplication
{
    public partial class _Default : Page
    {
        SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["connect"].ToString());
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                this.bindData();
                // GET Cust Count ---------------------------------------------------
                //string n = "";
                //SqlCommand sp_getCustCount = new SqlCommand();
                //sp_getCustCount.Connection = conn;
                //sp_getCustCount.CommandType = CommandType.StoredProcedure;
                //sp_getCustCount.CommandText = "countPage";
                //sp_getCustCount.Parameters.Add("@n", SqlDbType.VarChar, 30);
                //sp_getCustCount.Parameters["@n"].Direction = ParameterDirection.Output;
                //sp_getCustCount.ExecuteNonQuery();
                //n = sp_getCustCount.Parameters["@n"].Value.ToString();
                ////-----------------------------------------------------------------------
                //CustTable.VirtualItemCount = Convert.ToInt32(n);


            }

        }
        public void bindData()
        {
            try
            {
                conn.Open();
                SqlCommand sp_GetCust = new SqlCommand();
                sp_GetCust.Connection = conn;
                //sp_GetCust.CommandText = "spGetCustomer2";
                sp_GetCust.CommandType = CommandType.StoredProcedure;

                sp_GetCust.CommandText = "paging";
                int pageSize = 11;
                int pageNumber = 1;
                sp_GetCust.Parameters.AddWithValue("@PageSize", pageSize);
                sp_GetCust.Parameters.AddWithValue("@PageNumber", pageNumber);

                SqlDataReader reader = sp_GetCust.ExecuteReader();
                DataTable dt = new DataTable();
                dt.Load(reader);
                CustTable.DataSource = dt;
                CustTable.DataBind();

                ViewState["dirState"] = dt;
                ViewState["sortdr"] = "ASC";

                //string sql = "select * from Customers";
                //SqlCommand sqlCommand = new SqlCommand(sql, conn);
                //SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

            }
            catch (Exception ex)
            {
                Response.Write(ex.ToString());
            }
            finally
            {
                conn.Close();
            }
        }


        protected void CustTable_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            CustTable.PageIndex = e.NewPageIndex;
            this.bindData();
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            Response.Redirect("About.aspx");
        }


        protected void CustTable_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            try
            {
                conn.Open();
                GridViewRow row = CustTable.Rows[e.RowIndex];

                SqlCommand sp_Delete = new SqlCommand();
                

                string customerID = (row.FindControl("lbl_CustomerID") as Label).Text;

                sp_Delete.Connection = conn;
                sp_Delete.CommandText = "spDelCustomer";
                sp_Delete.CommandType = CommandType.StoredProcedure;

                sp_Delete.Parameters.AddWithValue("@CustomerID", customerID);
                if (sp_Delete.ExecuteNonQuery() == 1)
                {
                    Response.Write("<script>alert('Data Deletion Success');</script>");
                }
                else
                {
                    Response.Write("<script>alert('Data Deletion Failed');</script>");
                }
                
                //string sql = "delete from Customers where CustomerID='"+customerID+"'";
                //SqlCommand sqlCommand = new SqlCommand(sql, conn);
                //sqlCommand.ExecuteReader();
            }
            catch (Exception ex)
            {
                Response.Write(ex.ToString());
            }
            finally
            {
                conn.Close();
                this.bindData();
            }
        }


        protected void CustTable_RowEditing(object sender, GridViewEditEventArgs e)
        {
            CustTable.EditIndex = e.NewEditIndex;
            this.bindData();
        }

        protected void CustTable_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            try
            {
                conn.Open();
                GridViewRow row = CustTable.Rows[e.RowIndex];
                string customerID = (row.FindControl("lbl_CustomerID") as Label).Text;

                string companyName = (row.FindControl("tb_CompanyName") as TextBox).Text;
                string contactName = (row.FindControl("tb_ContactName") as TextBox).Text;
                string contactTitle = (row.FindControl("tb_ContactTitle") as TextBox).Text;
                string address = (row.FindControl("tb_Address") as TextBox).Text;
                string city = (row.FindControl("tb_City") as TextBox).Text;
                string region = (row.FindControl("tb_Region") as TextBox).Text;
                string postalCode = (row.FindControl("tb_PostalCode") as TextBox).Text;
                string country = (row.FindControl("tb_Country") as TextBox).Text;
                string phone = (row.FindControl("tb_Phone") as TextBox).Text;
                string fax = (row.FindControl("tb_Fax") as TextBox).Text;

                SqlCommand sp_Update = new SqlCommand();
                sp_Update.Connection = conn;
                sp_Update.CommandText = "spUpdateCustomer2";
                sp_Update.CommandType = CommandType.StoredProcedure;

                sp_Update.Parameters.AddWithValue("@CustomerID", customerID);
                sp_Update.Parameters.AddWithValue("@CompanyName", companyName);
                sp_Update.Parameters.AddWithValue("@ContactName", contactName);
                sp_Update.Parameters.AddWithValue("@ContactTitle", contactTitle);
                sp_Update.Parameters.AddWithValue("@Address", address);
                sp_Update.Parameters.AddWithValue("@City", city);
                sp_Update.Parameters.AddWithValue("@Region", region);
                sp_Update.Parameters.AddWithValue("@PostalCode", postalCode);
                sp_Update.Parameters.AddWithValue("@Country", country);
                sp_Update.Parameters.AddWithValue("@Phone", phone);
                sp_Update.Parameters.AddWithValue("@Fax", fax);
                if (sp_Update.ExecuteNonQuery() == 1)
                {
                    Response.Write("<script>alert('Data Update Success');</script>");
                }
                else
                {
                    Response.Write("<script>alert('Data Update Failed');</script>");
                }
                //string UpdateQuery = "Update Customers set " +
                //                                   "CompanyName = '" + companyName + "'," +
                //                                   "ContactName='" + contactName + "'," +
                //                                   "ContactTitle='" + contactTitle + "'," +
                //                                   "Address='" + address + "'," +
                //                                   "City='" + city + "'," +
                //                                   "Region='" + region + "'," +
                //                                   "PostalCode='" + postalCode + "'," +
                //                                   "Country='" + country + "'," +
                //                                   "Phone='" + phone + "'," +
                //                                   "Fax='" + fax +
                //                                   "' Where CustomerID ='" + customerID + "'";

                //SqlCommand sqlCommand = new SqlCommand(UpdateQuery, conn);
                //sqlCommand.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                Response.Write(ex.ToString());
                
            }
            finally
            {
                conn.Close();
                CustTable.EditIndex = -1; // Balik ke mode awal
                this.bindData();
                
            }
        }

        protected void CustTable_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            CustTable.EditIndex = -1;
            this.bindData();
        }

        protected void CustTable_Sorting(object sender, GridViewSortEventArgs e)
        {
            DataTable dt = (DataTable)ViewState["dirState"];
            if (dt != null)
            {
                if(Convert.ToString(ViewState["sortdr"]) == "Asc")
                {
                    dt.DefaultView.Sort = e.SortExpression + " Desc";
                    ViewState["sortdr"] = "Desc";
                }  
                else  
                {
                    dt.DefaultView.Sort = e.SortExpression + " Asc";
                    ViewState["sortdr"] = "Asc";
                }
                CustTable.DataSource = dt;
                CustTable.DataBind();
            }
        }

    }
}
