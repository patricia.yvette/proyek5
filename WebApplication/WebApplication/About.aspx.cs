﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApplication
{
    public partial class About : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {


//aabbcceedd

        }

        protected void btnInsert_Click(object sender, EventArgs e)
        {
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["connect"].ToString());
            try
            {
                conn.Open();
                SqlCommand sp_Insert = new SqlCommand();
                sp_Insert.Connection = conn;
                sp_Insert.CommandText = "spInsertCust";
                sp_Insert.CommandType = CommandType.StoredProcedure;

                sp_Insert.Parameters.AddWithValue("@CustomerID", Convert.ToString(CustomerID.Value));
                sp_Insert.Parameters.AddWithValue("@CompanyName", Convert.ToString(CompanyName.Value));
                sp_Insert.Parameters.AddWithValue("@ContactName", Convert.ToString(ContactName.Value));
                sp_Insert.Parameters.AddWithValue("@ContactTitle", Convert.ToString(ContactTitle.Value));
                sp_Insert.Parameters.AddWithValue("@Address", Convert.ToString(Address.Value));
                sp_Insert.Parameters.AddWithValue("@City", Convert.ToString(City.Value));
                sp_Insert.Parameters.AddWithValue("@Region", Convert.ToString(Region.Value));
                sp_Insert.Parameters.AddWithValue("@PostalCode", Convert.ToString(PostalCode.Value));
                sp_Insert.Parameters.AddWithValue("@Country", Convert.ToString(Country.Value));
                sp_Insert.Parameters.AddWithValue("@Phone", Convert.ToString(Phone.Value));
                sp_Insert.Parameters.AddWithValue("@Fax", Convert.ToString(Fax.Value));
                if (sp_Insert.ExecuteNonQuery() == 1)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect",
                    "alert('Data Insertion Success'); window.location='" +
                    Request.ApplicationPath + "Default.aspx';", true);
                }
                else
                {
                    Response.Write("<script>alert('Data Insertion Failed');</script>");
                }
                //string insertQuery = "insert into Customers" +
                //                "(CustomerID,CompanyName,ContactName,ContactTitle," +
                //                "Address,City,Region,PostalCode,Country,Phone,Fax)" +
                //                "values ("
                //                + "'" + CustomerID.Value + "',"
                //                + "'"+CompanyName.Value + "',"
                //                +"'"+ContactName.Value + "',"
                //                +"'"+ContactTitle.Value + "',"
                //                + "'" + Address.Value + "',"
                //                + "'" + City.Value + "',"
                //                + "'" + Region.Value + "',"
                //                + "'" + PostalCode.Value + "',"
                //                + "'" + Country.Value + "',"
                //                + "'" + Phone.Value + "',"
                //                + "'" + Fax.Value + "')";
                //SqlCommand sqlCommand = new SqlCommand(insertQuery, conn);

                //if (sqlCommand.ExecuteNonQuery()== 1)
                //{
                //    Response.Redirect("Default.aspx");
                //}
                //else
                //{
                //    //
                //}
            }
            catch (Exception ex)
            {
                Response.Write(ex.ToString());
            }
            finally
            {
                conn.Close();
            }
        }
    }
}